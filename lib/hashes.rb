# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  wl = Hash.new
  str.split.each { |s| wl[s] = s.length }
  wl
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.max_by { |k, v| v }[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  older.merge!(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter = Hash.new(0)
  word.chars.each { |c| counter[c] += 1 }
  counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  uniqs = []
  arr.each do |e|
    uniqs << e unless counter[e] > 0
    counter[e] += 1
  end
  uniqs
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  e_or_o = Hash.new(0)
  numbers.each do |i|
    if (i % 2).zero?
      e_or_o[:even] += 1
    else
      e_or_o[:odd] += 1
    end
  end
  e_or_o
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = %w[a e i o u]
  counter = Hash.new(0)
  string.chars.each do |c|
    counter[c] += 1 if vowels.include?(c)
  end
  largest = counter.values.max
  counter.to_a.select { |a| a.last == largest}.min_by { |a| a.first }.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.select { |k, v| v >= 7 }.keys.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter = Hash.new(0)
  specimens.each { |s| counter[s] += 1 }
  number_of_species = counter.length
  smallest_population_size = counter.min_by { |_, v| v }.last
  largest_population_size = counter.max_by { |_, v| v }.last
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = character_count(normal_sign)
  vandal_letters = character_count(vandalized_sign)
  vandal_letters.each { |k, v| return false if normal_letters[k] < v }
  true
end

def character_count(str)
  counter = Hash.new(0)
  str.downcase.gsub(/[^[:word:]]/, '').chars.each { |c| counter[c] += 1 }
  counter
end
